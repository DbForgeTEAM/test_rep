﻿CREATE TABLE [dbo].[CV] (
  [CVID] [int] IDENTITY,
  [1] [varbinary](max) NULL,
  [FName] [varchar](50) NULL,
  [FileExtension] [varchar](4) NULL,
  CONSTRAINT [PK_CVID] PRIMARY KEY CLUSTERED ([CVID])
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'test descrё', 'SCHEMA', N'dbo', 'TABLE', N'CV'
GO